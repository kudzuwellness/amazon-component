package com.kudzu.component.amazon.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.aws.ses.SesConstants;

import com.kudzu.commons.config.rest.AmazonREST;
import com.kudzu.commons.kiwi.model.EmailNotification;

public class EmailNotificationFilter implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		Class<?> clazz = exchange.getIn().getBody().getClass();
		
		if(clazz == EmailNotification.class){
			
			Message out = exchange.getOut();
			List<String> replyTo = new ArrayList<String>();
			EmailNotification notification = exchange.getIn().getMandatoryBody(EmailNotification.class);
			Map<String, String> props = notification.getProps();
			
			switch(notification.getType()){
				case INVITE_EMAIL:
					replyTo.add(AmazonREST.noreplyEmail);
					out.setHeader("email",        props.get("email"));
					out.setHeader("firstName",    props.get("firstName"));
					out.setHeader("lastName",     props.get("lastName"));
					out.setHeader("inviteLink",   props.get("inviteLink"));
					out.setHeader("clientId",     props.get("clientId"));
					out.setHeader("locationId",   props.get("locationId"));
					out.setHeader("clientName",   props.get("clientName"));
					out.setHeader("clientLogo",   props.get("clientLogo"));
					out.setHeader("programName",  props.get("programName"));
					out.setHeader("altName",      props.get("altName"));
					out.setHeader("displayLink",  props.get("displayLink"));
					out.setHeader("token",        props.get("token"));
					out.setHeader("emailFrom",    AmazonREST.noreplyEmail);
					out.setHeader("templateName", AmazonREST.inviteTemplate);
					out.setHeader("emailTo", notification.getTo());
					out.setHeader(SesConstants.REPLY_TO_ADDRESSES, replyTo);
					out.setHeader(SesConstants.SUBJECT, AmazonREST.inviteSubject);
					break;
				case CONTACT_EMAIL:
					if(props.get("emailRef") != null){
						replyTo.add(props.get("emailRef"));
					}
					out.setHeader("email", props.get("emailRef"));
					out.setHeader("message", notification.getMessage());
					out.setHeader("fullName", props.get("fullName"));
					out.setHeader("userId", props.get("userId"));
					out.setHeader("domain", props.get("domain"));
					out.setHeader("clientId", props.get("clientId"));
					out.setHeader("locationId", props.get("locationId"));
					out.setHeader("emailTo", AmazonREST.supportEmail);
					out.setHeader("emailFrom", AmazonREST.infoEmail);
					out.setHeader("templateName", AmazonREST.supportTemplate);
					out.setHeader(SesConstants.REPLY_TO_ADDRESSES, replyTo);
					out.setHeader(SesConstants.SUBJECT, props.get("topic"));
					break;
				case RESET_PASSWORD_EMAIL:
					replyTo.add(AmazonREST.noreplyEmail);
					out.setHeader("email",        props.get("email"));
					out.setHeader("firstName",    props.get("firstName"));
					out.setHeader("lastName",     props.get("lastName"));
					out.setHeader("linkBase",     props.get("linkBase"));
					out.setHeader("clientId",     props.get("clientId"));
					out.setHeader("locationId",   props.get("locationId"));
					out.setHeader("clientName",   props.get("clientName"));
					out.setHeader("clientLogo",   props.get("clientLogo"));
					out.setHeader("token",        props.get("token"));
					out.setHeader("emailFrom",    AmazonREST.noreplyEmail);
					out.setHeader("templateName", AmazonREST.forgotPasswordTemplate);
					out.setHeader("emailTo",      notification.getTo());
					out.setHeader(SesConstants.REPLY_TO_ADDRESSES, replyTo);
					out.setHeader(SesConstants.SUBJECT, AmazonREST.forgotPasswordSubject);
					break;
				case BADGE_NOTIFICATION_EMAIL:
					replyTo.add(AmazonREST.noreplyEmail);
					out.setHeader("email",            props.get("email"));
					out.setHeader("clientLogo",       props.get("clientLogo"));
					out.setHeader("badgeImgUrl",      props.get("badgeImgUrl"));
					out.setHeader("badgeName",        props.get("badgeName"));
					out.setHeader("badgeDescription", props.get("badgeDescription"));
					out.setHeader("profileLink",      props.get("linkToProfile"));
					out.setHeader("emailFrom",    AmazonREST.noreplyEmail);
					out.setHeader("templateName", AmazonREST.badgeTemplate);
					out.setHeader("emailTo", notification.getTo());
					out.setHeader(SesConstants.REPLY_TO_ADDRESSES, replyTo);
					out.setHeader(SesConstants.SUBJECT, AmazonREST.badgeSubject);
					break;
			}
			out.setHeader("access", AmazonREST.accessKey);
			out.setHeader("secret", AmazonREST.secretKey);
			out.setHeader(SesConstants.HTML_EMAIL, true);
		}
		
	}

}
