package com.kudzu.component.amazon.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.aws.ses.SesConstants;

import com.kudzu.commons.config.rest.AmazonREST;
import com.kudzu.component.amazon.schema.AmazonUserSupportEmail;



public class SupportEmailToHeader implements Processor {

	AmazonREST amazonRestConfig = new AmazonREST();
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		Class<?> clazz = exchange.getIn().getBody().getClass();
		
		if(clazz == AmazonUserSupportEmail.class){
			
			Message out = exchange.getOut();
			AmazonUserSupportEmail email = exchange.getIn().getMandatoryBody(AmazonUserSupportEmail.class);
			List<String> replyTo = new ArrayList<String>();
			
			if(email.getEmailRef() != null){
				replyTo.add(email.getEmailRef());
			}
			
			out.setHeader("email", email.getEmailRef());
			out.setHeader("message", email.getEmailMessage());
			out.setHeader("fullName", email.getFullName());
			out.setHeader("userId", email.getUserId());
			out.setHeader("clientId", email.getClientId());
			out.setHeader("locationId", email.getLocationId());
			out.setHeader("emailTo", amazonRestConfig.supportEmail);
			out.setHeader("emailFrom", amazonRestConfig.infoEmail);
			out.setHeader("templateName", amazonRestConfig.supportTemplate);
			out.setHeader("access", amazonRestConfig.accessKey);
			out.setHeader("secret", amazonRestConfig.secretKey);
			out.setHeader(SesConstants.REPLY_TO_ADDRESSES, replyTo);
			out.setHeader(SesConstants.SUBJECT, email.getTopic());
			out.setHeader(SesConstants.HTML_EMAIL, true);
			
		}
		
	}

}
