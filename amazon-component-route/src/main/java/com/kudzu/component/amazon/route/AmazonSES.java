package com.kudzu.component.amazon.route;

import javax.annotation.PostConstruct;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.model.rest.RestBindingMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.kudzu.commons.config.env.KiwiConfiguration;
import com.kudzu.commons.config.rest.AmazonREST;
import com.kudzu.commons.kiwi.model.ComponentNotification;
import com.kudzu.commons.kiwi.model.ComponentSource;
import com.kudzu.commons.kiwi.model.EmailNotification;
import com.kudzu.component.amazon.process.EmailNotificationFilter;
import com.kudzu.component.amazon.schema.AmazonNotification;

@Component
public class AmazonSES extends RouteBuilder {

	@Autowired
	KiwiConfiguration configuration;
	
	private static final Processor emailNotificationFilter = emailNotificationFilter();
	private static final JacksonDataFormat emailNotification = emailNotification();
	
	private static final String SIMPLE_AWS_URI = "aws-ses://${headers.emailFrom}?to=${headers.emailTo}&accessKey=${headers.access}&secretKey=${headers.secret}";
	
	private String SIMPLE_AWS_SQS_DELIVERY;
	private String SIMPLE_AWS_SQS_BOUNCE;
	
	static final Logger LOG = LoggerFactory.getLogger(AmazonSES.class);
	
	@PostConstruct
	public void init(){
		SIMPLE_AWS_SQS_DELIVERY = "aws-sqs://" + configuration.getKiwiSqsSesMailDeliveryQueue() + "?amazonSQSClient=#amazonSQSClient&delay=30000";
		SIMPLE_AWS_SQS_BOUNCE = "aws-sqs://" + configuration.getKiwiSqsSesMailBounceQueue() + "?amazonSQSClient=#amazonSQSClient&delay=30000";
	}
	
	@Override
	public void configure() throws Exception {

	    onException(Exception.class).handled(true).process(new Processor() {
	        @Override
	        public void process(Exchange exchange) {
	        	Exception exception = (Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
	        	exception.printStackTrace();
				exchange.getOut().setBody(exception.getMessage());
				exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "text/plain");
		        exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 403);
	        }
	    });
		
		restConfiguration()
			.component("servlet")
			.bindingMode(RestBindingMode.json)
			.dataFormatProperty("prettyPrint", "true")
			.contextPath(AmazonREST.context)
			.port(8080);
		
		rest(AmazonREST.contextRoot)
			.description("Amazon Services")
			.consumes("application/json")
			.produces("application/json")
			
			.get("/ping-ses")
				.description("Amazon-Ping")
				.to("direct:ping-amazon-ses")

			.post(AmazonREST.sendMail)
				.description("Add Entry Amazon")
				.to("direct:sendMailAmazon")
		
			.post(AmazonREST.sendSupportEmail)
				.description("Sends a support email currently set to support@growwell.com")
				.to("direct:send-support-email");

		
		from("direct:ping-amazon-ses")
			.routeId("Amazon-Ping-SES")
			.setBody()
			.constant("Ping Amazon success...");
		
		from("direct:send-support-email")
			.routeId("Amazon-SendSupportEmail")
			.unmarshal(emailNotification)
			.to("direct:email-notification");
		
		from("direct:email-notification")
			.routeId("Amazon-EmailNotification")
			.process(emailNotificationFilter)
			.setHeader("CamelVelocityResourceUri").simple("classpath:${headers.templateName}")
			.to("velocity:dummy")
			.recipientList(simple(SIMPLE_AWS_URI))
			.setBody().constant("ok");
		
		from(SIMPLE_AWS_SQS_DELIVERY)
			.log("RECEIVED NOTIFICATION: Amazon Email Delivery")
			.to("direct:preprocess-amazon-email-status-notification")
			.unmarshal(jacksonFormat(AmazonNotification.class))
			.wireTap("direct:amazon-component-notify-email-status")
		;
		
		from(SIMPLE_AWS_SQS_BOUNCE)
			.log("RECEIVED NOTIFICATION: Amazon Email Bounce")
			.to("direct:preprocess-amazon-email-status-notification")
			.unmarshal(jacksonFormat(AmazonNotification.class))
			.wireTap("direct:amazon-component-notify-email-status")
		;
		
		from("direct:preprocess-amazon-email-status-notification")
			.process(new Processor(){
				@Override
				public void process(Exchange exchange) throws Exception {
					Message in = exchange.getIn();
					Message out = exchange.getOut();
					out.setHeaders(in.getHeaders());
					String body = in.getBody(String.class);
					LOG.debug("Converting Amazon Notification Raw: {}", body);
					if(body != null){
						body = body.replaceFirst("\\\"Type\\\" :", "\\\"type\\\" :");
						body = body.replaceFirst("\\\"MessageId\\\" :", "\\\"messageId\\\" :");
						body = body.replaceFirst("\\\"TopicArn\\\" :", "\\\"topicArn\\\" :");
						body = body.replaceFirst("\\\"Message\\\" :", "\\\"message\\\" :");
						body = body.replaceFirst("\\\"Timestamp\\\" :", "\\\"timestamp\\\" :");
						body = body.replaceFirst("\\\"SignatureVersion\\\" :", "\\\"signatureVersion\\\" :");
						body = body.replaceFirst("\\\"Signature\\\" :", "\\\"signature\\\" :");
						body = body.replaceFirst("\\\"SigningCertURL\\\" :", "\\\"signingCertURL\\\" :");
						body = body.replaceFirst("\\\"UnsubscribeURL\\\" :", "\\\"unsubscribeURL\\\" :");
						body = body.replaceFirst("\\\"message\\\"\\s*:\\s*\\\"\\{(.*)\\}\\\",", "\\\"message\\\" : {$1},");
						body = body.replaceAll("\\\\\"{1}", "\\\"");
					}
					LOG.debug("Resulting Amazon Notification Formatted: {}", body);
					out.setBody(body, String.class);
				}
			})
		;
		
		
	    from("direct:amazon-component-notification-basic")
			.process(new Processor(){
				@Override
				public void process(Exchange exchange) throws Exception {
					Message in = exchange.getIn();
					Message out = exchange.getOut();
					AmazonNotification amazonNotify = in.getBody(AmazonNotification.class);
					ComponentNotification notification = new ComponentNotification();
					notification.setUserId(null);
					notification.setClientId(null);
					notification.setLocationId(null); 
					notification.setTimezone(null);
					notification.setSourceOrigin(ComponentSource.AMAZON);
					notification.setSourceOriginId(amazonNotify.getMessageId());
					notification.setSourceReporting(ComponentSource.AMAZON);
					notification.setSourceReportingId(amazonNotify.getMessageId());
					
					String type = "NA";
					String email = "";
					if(amazonNotify != null && amazonNotify.getMessage() != null){
						if(amazonNotify.getMessage().getNotificationType().equalsIgnoreCase("Delivery")){
							type = "DELIVERY";
							if(amazonNotify.getMessage().getMail().getDestination() != null && amazonNotify.getMessage().getMail().getDestination().size() > 0){
								email = amazonNotify.getMessage().getMail().getDestination().get(0);
							}
						}
						else if(amazonNotify.getMessage().getNotificationType().equalsIgnoreCase("Bounce")){
							type = "BOUNCE";
							if(amazonNotify.getMessage().getMail().getDestination() != null && amazonNotify.getMessage().getMail().getDestination().size() > 0){
								email = amazonNotify.getMessage().getMail().getDestination().get(0);
							}
						}
					}
					
					notification.addProperty("AmazonEmail", email);
					notification.addProperty("AmazonEmailStatusType", type);
					out.setHeaders(in.getHeaders());
					out.setBody(notification, ComponentNotification.class);
				}
			})  
		;
	    
	    from("direct:amazon-component-notify-email-status")
	    	.to("direct:amazon-component-notification-basic")
	    	.to("activemq:topic:Notification.Amazon.Email.Status")
	    ;
		
	}

	
	private static JacksonDataFormat emailNotification(){
		JacksonDataFormat format = new JacksonDataFormat();
		format.setUnmarshalType(EmailNotification.class);
		return format;
	}
	
	private static Processor emailNotificationFilter(){
		return new EmailNotificationFilter();
	}

    @SuppressWarnings("rawtypes")
	private static JacksonDataFormat jacksonFormat(Class clazz) {
        return jacksonFormat(clazz, false);
    }

    @SuppressWarnings("rawtypes")
    private static JacksonDataFormat jacksonFormat(Class clazz, boolean isList) {
        JacksonDataFormat format = new JacksonDataFormat();
        format.setUnmarshalType(clazz);
        format.setUseList(isList);
	    format.getObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	    format.getObjectMapper().configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
	    format.getObjectMapper().configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
        return format;
    }
	
}
