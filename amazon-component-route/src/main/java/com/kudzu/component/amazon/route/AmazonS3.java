package com.kudzu.component.amazon.route;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.aws.s3.S3Constants;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.shiro.codec.Base64;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.Permission;
import com.kudzu.commons.config.rest.AmazonREST;
import com.kudzu.commons.kiwi.model.Base64Image;
import com.kudzu.component.amazon.schema.AmazonFileEntry;

@Component
public class AmazonS3 extends RouteBuilder {

	private String destinationUri = "aws-s3://${headers.destinationBucket}?accessKey=AKIAIT34DMCSTXWMI2NQ&secretKey=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";

	@Override
	public void configure() throws Exception {
		restConfiguration()
			.component("servlet")
			.bindingMode(RestBindingMode.json)
			.dataFormatProperty("prettyPrint", "true")
			.contextPath(AmazonREST.context)
			.port(8080);
		
		rest(AmazonREST.contextRoot)
			.description("Amazon Services")
			.consumes("multipart/mixed")
			.produces("application/json")
			
			.get("/ping-s3")
				.description("Amazon-Ping")
				.to("direct:ping-amazon-s3")


			.post(AmazonREST.updateFile)
				.description("Update S3 File to Amazon")
				.type(AmazonFileEntry.class)
				.outType(AmazonFileEntry.class)
				.to("direct:AwsS3UpdateFile");



		//ping Amazon
		from("direct:ping-amazon-s3")
				.routeId("Amazon-Ping-S3")
				.transform()
				.constant("Ping Amazon success...");
		
		//AwsS3AddFile Amazon
		from("direct:AwsS3UpdateFile")
				.setHeader("uniqueId", simple(getContext().getUuidGenerator().generateUuid()))
				.setHeader("originalMessage",body())
				.setHeader("destinationBucket").simple("${body.destinationBucket}")
				.setHeader(S3Constants.KEY, simple("${body.targetLocation}/${headers.uniqueId}.${body.targetOriginalExtension}"))
				.setHeader(S3Constants.CONTENT_ENCODING, simple("${body.targetOriginalEncoding}"))
				.setBody(simple("${body.targetOriginalContent}"))
				.doTry()
					.to("direct:AwsS3ProcessRequest")
				.doCatch(Exception.class)
					.to("direct:AwsS3RequestFailed");
		
		from("direct:publish-base64-image").process(new Processor(){
			@Override
			public void process(Exchange exchange) throws Exception {
				Message out = exchange.getOut();
				Message in = exchange.getIn();
				Base64Image image = in.getMandatoryBody(Base64Image.class);
				byte[] imageBytes = Base64.decode(image.getBase64Image());
				AccessControlList acl = new AccessControlList();
				acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
				out.setHeaders(in.getHeaders());
				out.setHeader("destinationBucket", image.getTargetBase());
				out.setHeader(S3Constants.KEY, image.getTargetPath() + "/" + image.getFileName());
				out.setHeader(S3Constants.ACL, acl);
				out.setHeader(S3Constants.CONTENT_ENCODING, "hexBinary");
				out.setBody(imageBytes);
			}
		})
		.doTry()
			.to("direct:AwsS3ProcessRequest")
		.doCatch(Exception.class)
			.to("direct:AwsS3RequestFailed");

		from("direct:AwsS3ProcessRequest")
			.recipientList(simple(destinationUri));

		from("direct:AwsS3RequestFailed").process(new Processor(){
			@Override
			public void process(Exchange exchange) throws Exception {
				Exception exception = (Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
				exchange.getOut().setBody(exception.getMessage());
				exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "text/plain");
				exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 403); //Forbidden
			}
		});

		//.to("aws-s3://static.growwell.info&accessKey=AKIAIT34DMCSTXWMI2NQ&secretKey=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");


	}
	
}
