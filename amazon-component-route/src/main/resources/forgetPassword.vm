<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Password Reset</title>
		<style type="text/css">
			/* Client-specific Styles */
			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
			body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */
			
			/* Reset Styles */
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table td{border-collapse:collapse;}
			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

			/* Template Styles */

			/* ////////// STANDARD STYLING: COMMON PAGE ELEMENTS ////////// */

			/**
			* @tab Page
			* @section background color
			* @tip Set the background color for your email. You may want to choose one that matches your company's branding.
			*/
			body, #backgroundTable{
				/*@editable*/ background-color:#FAFAFA;
			}
			
			/**
			* @tab Page
			* @section heading 1
			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
			* @style heading 1
			*/
			h1, .h1{
				/*@editable*/ color:#505050;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:30px;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 2
			* @tip Set the styling for all second-level headings in your emails.
			* @style heading 2
			*/
			h2, .h2{
				/*@editable*/ color:#505050;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:24px;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 3
			* @tip Set the styling for all third-level headings in your emails.
			* @style heading 3
			*/
			h3, .h3{
				/*@editable*/ color:#505050;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:20px;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 4
			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
			* @style heading 4
			*/
			h4, .h4{
				/*@editable*/ color:#505050;
				display:block;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:16px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}
			
			/* ////////// STANDARD STYLING: PREHEADER ////////// */
			
			/**
			* @tab Header
			* @section preheader style
			* @tip Set the background color for your email's preheader area.
			*/
			#templatePreheader{
				/*@editable*/ background-color:#F5F5F5;
				/*@editable*/ border-bottom:1px solid #CCCCCC;
			}
			
			/**
			* @tab Header
			* @section preheader text
			* @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
			*/
			.preheaderContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:100%;
				/*@editable*/ text-align:left;
			}
			
			/**
			* @tab Header
			* @section preheader link
			* @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
			*/
			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#26abe2;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}
			
			/* ////////// STANDARD STYLING: HEADER ////////// */
			
			/**
			* @tab Header
			* @section upper header style
			* @tip Set the background color for your email's upper header area.
			*/
			#upperTemplateHeader{
				/*@editable*/ background-color:#FAFAFA;
				padding-top:30px;
				padding-bottom:30px;				
			}
			
			/**
			* @tab Header
			* @section upper header text
			* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
			*/
			.upperHeaderContent{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				/*@editable*/ padding-top:0;
				/*@editable*/ padding-right:0;
				/*@editable*/ padding-bottom:0;
				/*@editable*/ vertical-align:middle;
			}
			
			/**
			* @tab Header
			* @section upper header link
			* @tip Set the styling for your email's upper header links. Choose a color that helps them stand out from your text.
			*/
			.upperHeaderContent a:link, .upperHeaderContent a:visited, /* Yahoo! Mail Override */ .upperHeaderContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#26abe2;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:none;
			}
			
			/**
			* @tab Header
			* @section lower header style
			* @tip Set the background color for your email's lower header area.
			* @theme page
			*/
			#lowerTemplateHeader{
				/*@editable*/ background-color:#26abe2;
				padding-top:30px;
				padding-bottom:30px;
			}

			/**
			* @tab Header
			* @section lower header text
			* @tip Set the styling for your email's lower header text. Choose a size and color that is easy to read.
			*/
			.lowerHeaderContent{
				/*@editable*/ color:#FFFFFF;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:normal;
				/*@editable*/ line-height:100%;
				/*@editable*/ vertical-align:middle;
			}
			
			/**
			* @tab Header
			* @section lower header link
			* @tip Set the styling for your email's lower header links. Choose a color that helps them stand out from your text.
			*/
			.lowerHeaderContent a:link, .lowerHeaderContent a:visited, /* Yahoo! Mail Override */ .lowerHeaderContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#FFFFFF;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}
			
			#headerImage{
				height:auto;
				max-width:600px;
			}
			
			/* ////////// STANDARD STYLING: MAIN BODY ////////// */
			
			/**
			* @tab Body
			* @section body style
			* @tip Set the bottom border for your email's body area.
			* @theme footer
			*/
			#templateBodyWrapper{
				/*@editable*/ border-bottom:0px solid #0DB297;
			}

			/**
			* @tab Body
			* @section body text
			* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
			* @theme main
			*/
			.bodyContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}
			
			/**
			* @tab Body
			* @section body link
			* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
			*/
			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#0DB297;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}
			
			.bodyContent img{
				display:inline;
				height:auto;
			}

			/**
			* @tab Body
			* @section body button
			* @tip Set the styling for your email's main content button. Choose a size and color that draws attention.
			*/
			.rssButton{
				-moz-border-radius:5px;
				-webkitborder-radius:5px;
				/*@editable*/ background-color:#26ABE2;
				border-radius:5px;
			}

			/**
			* @tab Body
			* @section body button
			* @tip Set the styling for your email's main content button. Choose a size and color that draws attention.
			*/
			.rssButton, .rssButton a:link, .rssButton a:visited{
				/*@editable*/ color:#FFFFFF;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:12px;
				/*@editable*/ font-weight:bold;
				line-height:100%;
				text-decoration:none;
			}
			/* ////////// STANDARD STYLING: SIDEBAR ////////// */

			/**
			* @tab Sidebar
			* @section sidebar style
			* @tip Set the background color and border for your email's sidebar area.
			*/
			.sidebarContent{
				/*@editable*/ border-left:1px solid #DDDDDD;
			}

			/**
			* @tab Sidebar
			* @section sidebar text
			* @tip Set the styling for your email's sidebar text. Choose a size and color that is easy to read.
			*/
			.sidebarContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Sidebar
			* @section sidebar link
			* @tip Set the styling for your email's sidebar links. Choose a color that helps them stand out from your text.
			*/
			.sidebarContent div a:link, .sidebarContent div a:visited, /* Yahoo! Mail Override */ .sidebarContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#0DB297;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.sidebarContent img{
				display:inline;
				height:auto;
			}
			
			/* ////////// STANDARD STYLING: FOOTER ////////// */
			
			/**
			* @tab Footer
			* @section footer style
			* @tip Set the background color and top border for your email's footer area.
			* @theme footer
			*/
			#templateFooter{
				/*@editable*/ background-color:#222426;
				/*@editable*/ border-top:4px solid #FAFAFA;
			}
			
			/**
			* @tab Footer
			* @section footer text
			* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
			* @theme footer
			*/
			.footerContent div{
				/*@editable*/ color:#707070;
				/*@editable*/ font-family:Helvetica;
				/*@editable*/ font-size:11px;
				/*@editable*/ line-height:125%;
				/*@editable*/ text-align:left;
			}
			
			/**
			* @tab Footer
			* @section footer link
			* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
			*/
			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#909090;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}
			
			.footerContent img{
				display:inline;
			}
			
			/**
			* @tab Footer
			* @section social bar style
			* @tip Set the text alignment for your email's footer social bar.
			*/
			#social div{
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Footer
			* @section utility bar style
			* @tip Set the text alignment for your email's footer utility bar.
			*/
			#utility div{
				/*@editable*/ text-align:left;
			}
			
			#monkeyRewards img{
				max-width:190px;
			}

			#couponButton{
			/*@tab Body
			@section coupon button
			@tip Set the styling for your email's coupon button. Choose a size and color that stands out.*/
				moz-border-radius:10px;
				webkit-border-radius:10px;
				/*@editable*/background-color:#26ABE2;
				/*@editable*/border:0px solid #C84F3D;
				border-radius:10px;
			}
			/*
			@tab Body
			@section coupon button
			@tip Set the styling for your email's coupon button. Choose a size and color that stands out.
			*/
			#couponButton div,#couponButton div a:link,#couponButton div a:visited{
				/*@editable*/color:#FFFFFF;
				/*@editable*/font-family:Helvetica;
				/*@editable*/font-size:20px;
				/*@editable*/font-weight:bold;
				/*@editable*/line-height:100%;
				/*@editable*/text-align:center;
				/*@editable*/text-decoration:none;
				text-shadow:1px 1px 1px #5A2B40;
			}
		</style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #FAFAFA;width: 100%;">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="margin: 0;padding: 0;background-color: #FAFAFA;height: 100%;width: 100%;">
	            <tr>
                	<td align="center" valign="top" style="border-collapse: collapse;">

                        <!-- // Begin Template Preheader \ -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader" style="background-color: #F5F5F5;border-bottom: 1px solid #CCCCCC;">
                        	<tr>
                            	<td align="center" valign="top" class="preheaderContent" style="border-collapse: collapse;">
                                
                                	<!-- // Begin Preheader Content \ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600">
                                    	<tr>
                                        	<td valign="top" width="400" style="padding-top: 15px;padding-right: 20px;padding-bottom: 15px;padding-left: 0;border-collapse: collapse;">
                                            	<div mc:edit="std_preheader_content" style="color: #505050;font-family: Helvetica;font-size: 10px;line-height: 100%;text-align: left;">
                                                	 We Have Receieved A Request To Reset Your Password
                                                </div>
                                            </td>
                                            
                                        </tr>
                                    </table>
                                    <!-- // Begin Preheader Content \ -->
                                    
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \ -->
                        
                    </td>
    	        </tr>
                <tr>
                	<td align="center" valign="top" style="border-collapse: collapse;">

                        <!-- // Begin Template Header \ -->
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                        	<tr>
                            	<td align="center" valign="top" id="upperTemplateHeader" style="border-collapse: collapse;background-color: #FAFAFA;padding-top: 30px;padding-bottom: 30px;">
                                
                                	<!-- // Begin Upper Header Content \ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600">
                                    	<tr>
                                        	<td valign="top" class="upperHeaderContent" style="padding-right: 20px;border-collapse: collapse;color: #505050;font-family: Helvetica;font-size: 34px;font-weight: normal;line-height: 100%;padding-top: 0;padding-bottom: 0;vertical-align: middle;">
                                            	<img src="${headers.clientLogo}" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext style="border: 0;line-height: 100%;outline: none;text-decoration: none;">
                                            </td>
                                            <td align="center" valign="bottom" width="18" mc:hideable style="padding-right: 5px;border-collapse: collapse;" mc:edit="social_link00">
                                                <a href="https://twitter.com/kudzuwellness" target="_blank"><img src="http://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/twitter_24x24_p50.png" alt="Follow on Twitter" style="max-width: 18px !important;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a>
                                            </td>
                                            <td align="center" valign="bottom" width="18" mc:hideable mc:edit="social_link01" style="border-collapse: collapse;">
                                                <a href="https://www.facebook.com/KudzuWellness" target="_blank"><img src="http://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/facebook_24x24_p50.png" alt="Friend of Facebook" style="max-width: 18px !important;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"></a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End  Upper Header Content \ -->
                                
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" id="lowerTemplateHeader" style="border-collapse: collapse;background-color: #26abe2;padding-top: 30px;padding-bottom: 30px;">

                                	<!-- // Begin Lower Header Content \ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600">
                                    	<tr>
                                        	<td valign="top" class="lowerHeaderContent" style="border-collapse: collapse;color: #FFFFFF;font-family: Helvetica;font-size: 34px;font-weight: normal;line-height: 100%;vertical-align: middle;">
                                            	<div mc:edit="upper_header_content">
                                                	Reset Password Request
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End  Lower Header Content \ -->

                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Header \ -->
                    
                    </td>
                </tr>
                <tr>
                	<td align="center" valign="top" style="padding-bottom: 40px;border-collapse: collapse;border-bottom: 0px solid #0DB297;" id="templateBodyWrapper">

                        <!-- // Begin Template Body \ -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600">
                        	<tr>
                            	<td colspan="2" width="600" style="border-collapse: collapse;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td valign="top" class="bodyContent" style="padding-top: 20px;border-collapse: collapse;">
                                                <div mc:edit="std_content00" style="color: #505050;font-family: Helvetica;font-size: 14px;line-height: 150%;text-align: left;">
                                                    <h1 class="h1" style="color: #505050;display: block;font-family: Helvetica;font-size: 30px;font-weight: normal;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">You Requested To Reset Your Password</h1>
                                                    We have received a request to reset your password. To reset your password, click the link below. If you think you have received this email in error, or do not wish to reset your password, please ignore this email.
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <br>
                            <tr>
                               <td align="center" colspan="2" valign="middle" style="padding-top: 20px;padding-bottom: 20px;border-collapse: collapse;">
                                    <table border="0" cellpadding="10" cellspacing="0" id="couponButton" style="moz-border-radius: 10px;webkit-border-radius: 10px;background-color: #26ABE2;border: 0px solid #C84F3D;border-radius: 10px;">
                                        <tr>
                                            <td align="center" valign="middle" style="padding-right: 20px;padding-left: 20px;border-collapse: collapse;">
                                                <div mc:edit="coupon_button" style="color: #FFFFFF;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;text-align: center;text-decoration: none;text-shadow: 1px 1px 1px #5A2B40;">
                                                    <a href="${headers.linkBase}/password/reset/${headers.token}" target="_blank" style="color: #FFFFFF;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;text-align: center;text-decoration: none;text-shadow: 1px 1px 1px #5A2B40;">Reset Your Password</a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Body \ -->
                    
                    </td>
                </tr>
                <tr>
                	<td align="center" valign="top" style="border-collapse: collapse;">
                    
                        <!-- // Begin Template Footer \ -->
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter" style="background-color: #222426;border-top: 4px solid #FAFAFA;">
                        	<tr>
                            	<td align="center" valign="top" style="padding-top: 40px;padding-bottom: 40px;border-collapse: collapse;">
                                
                                	<!-- // Begin Footer Content \ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" class="footerContent">
                                        <tr>
                                            <td colspan="2" valign="middle" id="social" style="border-collapse: collapse;">
                                                <div mc:edit="std_social" style="color: #707070;font-family: Helvetica;font-size: 11px;line-height: 125%;text-align: left;">
                                                    <a href="twitter.com/kudzuwellness" target="_blank" style="color: #909090;font-weight: normal;text-decoration: underline;">follow on Twitter</a> | <a href="https://www.facebook.com/KudzuWellness" target="_blank" style="color: #909090;font-weight: normal;text-decoration: underline;">like on Facebook</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="400" style="padding-top: 20px;padding-right: 20px;padding-bottom: 20px;padding-left: 0;border-collapse: collapse;">
                                                <div mc:edit="std_footer" style="color: #707070;font-family: Helvetica;font-size: 11px;line-height: 125%;text-align: left;">
                                                    <em>Copyright &copy; 2014 Kudzu Wellness Inc, All rights reserved.</em>
                                                    <br>
                                                    <br>
  													<strong>
                                                    3824 North Elm Street
                                                    <br>
                                                    Greensboro, NC
                                                    </strong>
                                                </div>
                                            </td>
                                            <td valign="right" class="footerContent" style="padding-right:0px;"><img src="http://static.growwell.com/resources/img/logos/email/kudzu_logo_email.png" id="headerImage campaign-icon" mc:label="footer_image" mc:allowdesigner mc:allowtext />
                                            </td>
                                        </tr>
                                        
                                    </table>
                                    <!-- // End Footer Content \ -->
                                
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Footer \ -->
                    
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>