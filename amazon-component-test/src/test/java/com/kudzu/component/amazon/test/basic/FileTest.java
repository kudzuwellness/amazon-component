package com.kudzu.component.amazon.test.basic;

import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.aws.s3.S3Constants;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.JndiRegistry;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.kudzu.component.amazon.schema.AmazonFileEntry;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by jgazeta on 12/11/14.
 */

//public class FileTest extends CamelTestSupport {
//
//    @EndpointInject(uri = "mock:result")
//    protected MockEndpoint resultEndpoint;
//
//    @Produce(uri = "direct:start")
//    protected ProducerTemplate template;
//
//    CamelContext registry = new DefaultCamelContext();
//
//    @Test
//    public void testSendNotMatchingMessage() throws Exception {
//        resultEndpoint.expectedMessageCount(0);
//        AmazonFileEntry fileEntry = new AmazonFileEntry();
//        fileEntry.setTargetLocation("resources/test"); //set the context for that bucket
//        fileEntry.setDestinationBucket("static.growwell.info");
//        fileEntry.setTargetOriginalExtension("png"); //pass extension
//        fileEntry.setTargetOriginalEncoding("hexBinary"); //you can change this to base64 if desired as well
//
//        Path file =  Paths.get("/Users/jgazeta/Documents/sandbox/projects/tmp/front-end-design/static/img/logo.png");
//        byte[] fileArray;
//        fileArray = Files.readAllBytes(file);
//        fileEntry.setTargetOriginalContent(fileArray);
//        template.sendBody(fileEntry);
//
//
//        resultEndpoint.assertIsSatisfied();
//    }
//
//    private String destinationUri = "aws-s3://${headers.destinationBucket}?accessKey=AKIAIT34DMCSTXWMI2NQ&secretKey=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";
//    protected RouteBuilder createRouteBuilder() {
//        return new RouteBuilder() {
//            public void configure() {
//
//                from("direct:start")
//                        .setHeader("originalMessage", body())
//                        .setHeader("destinationBucket").simple("${body.destinationBucket}")
//                        .setHeader(S3Constants.KEY, simple("${body.targetLocation}/${headers.uniqueId}.${body.targetOriginalExtension}"))
//                        .setHeader(S3Constants.CONTENT_ENCODING, simple("${body.targetOriginalEncoding}"))
//                        .setBody(simple("${body.targetOriginalContent}"))
//                        .doTry()
//                        .to("direct:AwsS3ProcessRequest")
//                        .doCatch(Exception.class)
//                        .to("direct:AwsS3RequestFailed");
//
//                from("direct:AwsS3ProcessRequest")
//                        .recipientList(simple(destinationUri))
//                        .setBody(simple("${headers.originalMessage}"));
//
//                from("direct:AwsS3RequestFailed").process(new Processor(){
//                    @Override
//                    public void process(Exchange exchange) throws Exception {
//                        exchange.getOut().setBody("An error occured with processing your request."); //hardcoded for now, should come from the exception message
//                        exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "text/plain");
//                        exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 400); //Bad Request
//                    }
//                });
//
//            }
//        };
//    }
//
//
//
//
//}
