//package com.kudzu.component.amazon.test.basic;
//
//import org.apache.camel.CamelContext;
//import org.apache.camel.EndpointInject;
//import org.apache.camel.Produce;
//import org.apache.camel.ProducerTemplate;
//import org.apache.camel.builder.RouteBuilder;
//import org.apache.camel.component.mock.MockEndpoint;
//import org.apache.camel.impl.DefaultCamelContext;
//import org.apache.camel.impl.JndiRegistry;
//import org.apache.camel.test.junit4.CamelTestSupport;
//import org.junit.Test;
//
//import com.amazonaws.ClientConfiguration;
//import com.amazonaws.auth.AWSCredentials;
//import com.amazonaws.auth.BasicAWSCredentials;
//import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
//import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
//
///**
// *
// * Created by jgazeta on 12/13/14.
// *
// */
//
//public class SmtpTest extends CamelTestSupport {
//
//    @EndpointInject(uri = "mock:result")
//    protected MockEndpoint resultEndpoint;
//
//    @Produce(uri = "direct:start")
//    protected ProducerTemplate template;
//
//    CamelContext registry = new DefaultCamelContext();
//
//
//    //@Test
//    public void testSendNotMatchingMessage() throws Exception {
//        resultEndpoint.expectedMessageCount(0);
//
//        template.sendBody("this is a test");
//
//        resultEndpoint.assertIsSatisfied();
//    }
//
//    protected RouteBuilder createRouteBuilder() {
//
//        //TODO - needs to implement new SSL authentication and encode the string so that this is viable.
//        return new RouteBuilder() {
//            public void configure() {
//
//                from("direct:start")
//                        .transform().constant("this is a test")
//                .to("smtps://smtp.gmail.com?username=gazetaj@gmail.com&password=XXXXXXX&to=joao@growwell.com");
//            }
//        };
//    }
//
//    @Override
//
//    protected JndiRegistry createRegistry() throws Exception {
//
//        JndiRegistry registry = super.createRegistry();
//
//
//        AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAIT34DMCSTXWMI2NQ", "E9eHk3a6O/pLIZ2ROWd4aNNdgBxLfLsj6vKjYzYW");
//
//        ClientConfiguration clientConfiguration = new ClientConfiguration();
//
////        clientConfiguration.setProxyHost("http://myProxyHost");
////        clientConfiguration.setProxyPort(8080);
//
//
//        AmazonSimpleEmailService client = new AmazonSimpleEmailServiceClient(awsCredentials, clientConfiguration);
//
//
//        registry.bind("amazonSesClient", client);
//
//
//        return registry;
//
//    }
//}
////.to("smtp://someone@localhost?password=secret&to=incident@mycompany.com");