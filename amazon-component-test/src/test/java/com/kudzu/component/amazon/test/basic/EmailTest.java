//package com.kudzu.component.amazon.test.basic;
//
//import org.apache.camel.CamelContext;
//import org.apache.camel.EndpointInject;
//import org.apache.camel.Produce;
//import org.apache.camel.ProducerTemplate;
//import org.apache.camel.builder.RouteBuilder;
//import org.apache.camel.component.aws.ses.SesConstants;
//import org.apache.camel.component.mock.MockEndpoint;
//import org.apache.camel.impl.DefaultCamelContext;
//import org.apache.camel.impl.JndiRegistry;
//import org.apache.camel.test.junit4.CamelTestSupport;
//import org.junit.Test;
//
//import com.amazonaws.ClientConfiguration;
//import com.amazonaws.auth.AWSCredentials;
//import com.amazonaws.auth.BasicAWSCredentials;
//import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
//import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
//import com.kudzu.component.amazon.schema.AmazonInviteEmailEntry;
//
///**
// * Created by jgazeta on 12/11/14.
// */
//
//public class EmailTest extends CamelTestSupport {
//
//    @EndpointInject(uri = "mock:result")
//    protected MockEndpoint resultEndpoint;
//
//    @Produce(uri = "direct:start")
//    protected ProducerTemplate template;
//
//    CamelContext registry = new DefaultCamelContext();
//
////    @Test
////    public void testSendMatchingMessage() throws Exception {
////        String expectedBody = "<matched/>";
////
////        resultEndpoint.expectedBodiesReceived(expectedBody);
////
////        //template.sendBodyAndHeader(expectedBody, "foo", "bar");
////
////        //resultEndpoint.assertIsSatisfied();
////    }
//
//    @Test
//    public void testSendNotMatchingMessage() throws Exception {
//        resultEndpoint.expectedMessageCount(0);
//
//        AmazonInviteEmailEntry amazonInviteEmailEntry = new AmazonInviteEmailEntry();
//
//        amazonInviteEmailEntry.setCompanyLogo("test");
//        amazonInviteEmailEntry.setEmailFrom("info@growwell.com");
//        amazonInviteEmailEntry.setEmailTo("joao@growwell.com");
//        amazonInviteEmailEntry.setEmailSubject("this is an invite");
//        amazonInviteEmailEntry.setTemplateName("inviteToTeam.vm");
//
//        amazonInviteEmailEntry.setEmailLink("test");
//        amazonInviteEmailEntry.setTeamName("test");
//        amazonInviteEmailEntry.setTeamDes("test");
//        amazonInviteEmailEntry.setTeamOwnerFullName("Test Test");
//
//
//        amazonInviteEmailEntry.setTeamOwnerImage("test");
//
//
//
//        template.sendBody(amazonInviteEmailEntry);
//
//        resultEndpoint.assertIsSatisfied();
//    }
//
//    protected RouteBuilder createRouteBuilder() {
//        return new RouteBuilder() {
//            public void configure() {
//
//
//                String destinationUri = "aws-ses://info@growwell.com?to=${headers.emailTo}&amazonSESClient=#amazonSesClient";
//
//                from("direct:start")
//
//                        .setHeader("originalMessage", body().convertTo(String.class))
//                        .setHeader("companyLogo", simple("${body.companyLogo}"))
//                        .setHeader("teamName",simple("${body.teamName}"))
//                        .setHeader("teamDes", simple("${body.teamDes}"))
//                        .setHeader("teamOwnerImage", simple("${body.teamOwnerImage}"))
//                        .setHeader("teamOwnerFullName", simple("${body.teamOwnerFullName}"))
//                        .setHeader("emailTo", simple("${body.emailTo}"))
//                        .setHeader("emailLink", simple("${body.emailLink}"))
//                        .setHeader("templateName", simple("${body.templateName}"))
//                        .setHeader(SesConstants.FROM, simple("${body.emailFrom}"))
//                                //this should replace the destination, but it doesn't work properly - this will override when a list is actually placed in
//                        //.setHeader(SesConstants.TO, simple("joao@growwell.com", String.class))
//                        .setHeader(SesConstants.SUBJECT, simple("${body.emailSubject}"))
//                        .setHeader(SesConstants.HTML_EMAIL, simple("true", Boolean.class))
//                                //setup template and convert  message to template
//                        .setHeader("CamelVelocityResourceUri").simple("classpath:${headers.templateName}")
//                        .to("velocity:dummy")
//                                //"aws-ses://info@growwell.com?to=joao@growwell.com&accessKey=AKIAIT34DMCSTXWMI2NQ&secretKey=E9eHk3a6O/pLIZ2ROWd4aNNdgBxLfLsj6vKjYzYW"
//                        .recipientList(simple(destinationUri));
//
//            }
//        };
//    }
//
//    @Override
//
//    protected JndiRegistry createRegistry() throws Exception {
//
//        JndiRegistry registry = super.createRegistry();
//
//
//        AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAIT34DMCSTXWMI2NQ", "E9eHk3a6O/pLIZ2ROWd4aNNdgBxLfLsj6vKjYzYW");
//
//        ClientConfiguration clientConfiguration = new ClientConfiguration();
//
//        AmazonSimpleEmailService client = new AmazonSimpleEmailServiceClient(awsCredentials, clientConfiguration);
//
//
//        registry.bind("amazonSesClient", client);
//
//
//        return registry;
//
//    }
//}
