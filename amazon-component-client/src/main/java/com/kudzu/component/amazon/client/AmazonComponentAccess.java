package com.kudzu.component.amazon.client;

import com.kudzu.commons.kiwi.model.EmailNotification;
import com.kudzu.commons.web.info.Requestor;
import com.kudzu.component.amazon.schema.AmazonFileEntry;
import com.kudzu.component.amazon.schema.AmazonUserSupportEmail;

public interface AmazonComponentAccess {

	public String getAmazonPing(Requestor requestor);

	public String sendEmailAmazon(Requestor requestor);

	public AmazonFileEntry updateFileAmazon(String fileId, Requestor requestor);

	public AmazonFileEntry addFileAmazon(String fileId, Requestor requestor);
	
	public String sendSupportEmail(EmailNotification email, Requestor requestor);

}
