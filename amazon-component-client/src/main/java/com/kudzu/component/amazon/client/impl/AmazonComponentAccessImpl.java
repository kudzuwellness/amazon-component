package com.kudzu.component.amazon.client.impl;

import java.io.Serializable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kudzu.commons.config.env.KiwiConfiguration;
import com.kudzu.commons.config.rest.AmazonREST;
import com.kudzu.commons.kiwi.model.EmailNotification;
import com.kudzu.commons.web.client.JerseyClientFactory;
import com.kudzu.commons.web.info.Requestor;
import com.kudzu.component.amazon.client.AmazonComponentAccess;
import com.kudzu.component.amazon.schema.AmazonFileEntry;
import com.kudzu.component.amazon.schema.AmazonUserSupportEmail;

@Component
public class AmazonComponentAccessImpl implements AmazonComponentAccess, Serializable {

	private static final long serialVersionUID = 1L;
	private static final Client client = JerseyClientFactory.createClient("AmazonManager");

	@Autowired
	KiwiConfiguration configuration;

	public AmazonComponentAccessImpl(){}
	
	public WebTarget getBaseTarget(Requestor requestor) {
		WebTarget target = client.target(configuration.getKiwiPlatformURI()).path(AmazonREST.context);
		return target;
	}

	public  String getAmazonPing(Requestor requestor) {
		WebTarget target = getBaseTarget(requestor).path(AmazonREST.ping);
		return target.request(MediaType.APPLICATION_JSON).get(String.class);
	}

	public  String sendEmailAmazon(Requestor requestor) {
		WebTarget target = getBaseTarget(requestor).path(AmazonREST.getFileInterface);
		return target.request(MediaType.APPLICATION_JSON).get(String.class);
	}

	public AmazonFileEntry updateFileAmazon(String fileId, Requestor requestor) {
		WebTarget target = getBaseTarget(requestor).path(AmazonREST.updateFileInterface);
		return target.request(MediaType.APPLICATION_JSON).post(Entity.json(AmazonFileEntry.class), AmazonFileEntry.class);
	}

	public AmazonFileEntry addFileAmazon(String fileId, Requestor requestor) {
		WebTarget target = getBaseTarget(requestor).path(AmazonREST.addFileInterface);
		return target.request(MediaType.APPLICATION_JSON).put(Entity.json(AmazonFileEntry.class), AmazonFileEntry.class);
	}
	
	public String sendSupportEmail(EmailNotification email, Requestor requestor) throws RuntimeException {
		WebTarget target = getBaseTarget(requestor).path(AmazonREST.sendSupportEmail);
		Response response = target.request(MediaType.APPLICATION_JSON).post(Entity.json(email));
		if(response.getStatus() != Response.Status.OK.getStatusCode()){
			throw new RuntimeException(response.readEntity(String.class));
		}
		else{
			return response.readEntity(String.class);
		}
	}

}
